
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CloneManagerComponent } from './components/clone-manager/clone-manager.component';
import { SyncManagerComponent } from './components/sync-manager/sync-manager.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'clone',
        pathMatch: 'full'
    },
    {
        path: 'clone',
        component: CloneManagerComponent
    },
    {
        path: 'sync',
        component: SyncManagerComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
