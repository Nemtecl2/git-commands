import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ElectronService } from './electron.service';

@Injectable()
export class PathService {
    path: any;
    pathSubject = new Subject<any>();


    constructor(private electronService: ElectronService) {
        this.path = this.electronService.process.cwd();
        this.emitPathSubject();
    }

    emitPathSubject() {
        this.pathSubject.next(this.path);
    }

    setPath(newPath) {
        this.path = newPath;
        this.electronService.process.chdir(this.path);
        this.emitPathSubject();
    }
}
