import { Component, OnInit } from '@angular/core';
import { ElectronService } from '../../providers/electron.service';
import { PathService } from '../../providers/path.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import * as git from 'isomorphic-git';

@Component({
  selector: 'app-sync-manager',
  templateUrl: './sync-manager.component.html',
  styleUrls: ['./sync-manager.component.scss']
})

export class SyncManagerComponent implements OnInit {
  action: string;

  constructor(private electronService: ElectronService,
    private pathService: PathService,
    private toastrService: ToastrService) { }

  ngOnInit() {
    this.onChangeRadioSync('Pull');
  }

  async onSubmit(form: NgForm) {
    const dir = this.pathService.path;
    console.log(dir);
    /*let branch = await git.currentBranch({ fs: this.electronService.fs, dir: dir, fullname: false });
    console.log(branch);*/

    // A FAIRE POUR PULL
    if (this.action == 'Pull') {
      await git.pull({
        fs: this.electronService.fs,
        dir: dir,
        username: form.value['username'],
        password: form.value['password']//,
        // ref: branch,
        // singleBranch: true
      });
    } /*else if (this.action == 'Pull') {
      await git.push({
        fs: this.electronService.fs
        dir: dir,
        username: form.value['username'],
        password: form.value['password']//,
        //ref: branch
      });
    }*/
  }

  onChangeRadioSync(newAction: string) {
    this.action = newAction;
  }
}