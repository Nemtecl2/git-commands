import { Component, OnInit } from '@angular/core';
import { ElectronService } from '../../providers/electron.service';
import { PathService } from '../../providers/path.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import * as git from 'isomorphic-git';

@Component({
  selector: 'app-clone-manager',
  templateUrl: './clone-manager.component.html',
  styleUrls: ['./clone-manager.component.scss']
})
export class CloneManagerComponent implements OnInit {
  test: any;
  constructor(private electronService: ElectronService,
    private pathService: PathService,
    private toastrService: ToastrService) { }

  ngOnInit() {
  }

  async onSubmit(form: NgForm) {
   this.test = this.electronService.childProcess.execSync('git log --oneline --graph --decorate').toString();
    // const regex = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
    //  if (!form.value['url'].match(regex)) {
    //    this.toastrService.error('Veuillez saisir une URL correcte');
    //  } else {
    //   // Problème avec les lien finissant en / (ex : https://github.com/isomorphic-git/isomorphic-git/)
    //   const dirname = form.value['url'].replace(/\.git$/, '').replace(/.*\//, '');
    //   const dir = this.pathService.path + '/' + dirname;
    //   this.electronService.fs.mkdir(`${this.pathService.path}/${dirname}`, async (err) => {
    //     if (err) {
    //       console.log(err);
    //       console.log(this);
    //     } else {
    //       try {
    //         await git.clone({
    //           fs: this.electronService.fs,
    //           dir: dir,
    //           url: form.value['url'],
    //           ref: 'master',
    //           username: form.value['identifiant'],
    //           password: form.value['password']
    //         });
    //         console.log('avant');
    //         // Probleme ici
    //         this.pathService.setPath(dir);
    //         console.log('apres');
    //       } catch (err) {
    //         console.log(err);
    //       }
    //     }
    //   });
    // }
  }
}
