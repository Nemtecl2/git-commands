import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloneManagerComponent } from './clone-manager.component';

describe('CloneManagerComponent', () => {
  let component: CloneManagerComponent;
  let fixture: ComponentFixture<CloneManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloneManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloneManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
