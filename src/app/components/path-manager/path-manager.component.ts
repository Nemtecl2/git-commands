import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ElectronService } from '../../providers/electron.service';
import { PathService } from '../../providers/path.service';

@Component({
  selector: 'app-path-manager',
  templateUrl: './path-manager.component.html',
  styleUrls: ['./path-manager.component.scss']
})
export class PathManagerComponent implements OnInit, OnDestroy {
  path: any;
  pathSubscription: Subscription;


  constructor(private electronService: ElectronService,
    private pathService: PathService) { 
      this.pathSubscription = this.pathService.pathSubject.subscribe(
        (path: any) => {
          this.path = path;
        }
      );
      this.pathService.emitPathSubject();
  }

  ngOnInit() {
  }

  changePath() {
    const newPath = this.electronService.remote.dialog.showOpenDialog({
      properties: ['openDirectory']
    });
    if (newPath !== undefined) {
      this.pathService.setPath(newPath[0]);
    }
  }

  ngOnDestroy() {
    this.pathSubscription.unsubscribe();
  }
}
