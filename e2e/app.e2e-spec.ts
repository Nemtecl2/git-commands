import { GitCommandsPage } from './app.po';
import { browser, element, by } from 'protractor';

describe('git-commands App', () => {
  let page: GitCommandsPage;

  beforeEach(() => {
    page = new GitCommandsPage();
  });

  it('should display message saying App works !', () => {
    page.navigateTo('/');
    expect(element(by.css('app-home h1')).getText()).toMatch('App works !');
  });
});
