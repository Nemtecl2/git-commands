import { browser, element, by } from 'protractor';

/* tslint:disable */
export class GitCommandsPage {
  navigateTo(route: string) {
    return browser.get(route);
  }
}
